const initialState = {
    result: ''
};

const reducer = (state = initialState, action) => {
    if (action.type === 'SHOW_RESULT') {
        return {...state, result: eval(state.result)};
    };
    if (action.type === 'ADD_NUMBER') {
        return {...state, result: state.result + action.value}
    };
    if (action.type === 'RESET') {
        return {...state, result: state.result = 0}
    };

    return state;
};


export default reducer;