import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import './Calculator.css';

const Calculator = () => {
    const counter = useSelector(state => state.result);
    const dispatch = useDispatch();

    const onClickHandler = (num) => {
        dispatch({type: 'ADD_NUMBER', value: num});
    };

    const getResult = () => {
        dispatch({type: 'SHOW_RESULT'});
    };

    const reset = () => {
        dispatch({type: 'RESET'});
    };

    return (
        <div className="Calculator">
            <form className="calculator-form">
                <div className="col">
                    <input type="button" value="1" className="num" onClick={()=>onClickHandler(1)}/>
                    <input type="button" value="2"  className="num" onClick={()=>onClickHandler(2)}/>
                    <input type="button" value="3"  className="num" onClick={()=>onClickHandler(3)}/>
                    <input type="button" value="+"  className="num" onClick={()=>onClickHandler('+')}/>
                </div>
                <div className="col">
                    <input type="button" value="4"  className="num" onClick={()=>onClickHandler(4)}/>
                    <input type="button" value="5"  className="num" onClick={()=>onClickHandler(5)}/>
                    <input type="button" value="6"  className="num" onClick={()=>onClickHandler(6)}/>
                    <input type="button" value="-"  className="num" onClick={()=>onClickHandler('-')}/>
                </div>
                <div className="col">
                    <input type="button" value="7"  className="num" onClick={()=>onClickHandler(7)}/>
                    <input type="button" value="8"  className="num" onClick={()=>onClickHandler(8)}/>
                    <input type="button" value="9"  className="num" onClick={()=>onClickHandler(9)}/>
                    <input type="button" value="*"  className="num" onClick={()=>onClickHandler('*')}/>
                </div>
                <div className="col">
                    <input type="button" value="="  className="num" onClick={getResult}/>
                    <input type="button" value="0"  className="num" onClick={()=>onClickHandler(0)}/>
                    <input type="button" value="."  className="num" onClick={()=>onClickHandler('.')}/>
                    <input type="button" value="/"  className="num" onClick={()=>onClickHandler('/')}/>
                </div>
                <div className="col-1 col">
                    <input type="reset" value="Reset" className="reset" onClick={reset}/>
                    <input type="text" name="ans" className="field" value={counter} readOnly="readonly"/>
                </div>
            </form>
        </div>
    );
};

export default Calculator;